# CarCar

Team:

* Terence Wong - Sales Microservice
* Ash Avila - Service Microservice

## Steps to run the application:

1. Open Docker desktop and terminal
2. Clone the project using command git clone (link)
3. Run "docker volume create beta-data"
4. Run "docker-compose build"
5. Run "docker-compose up"
6. Ensure all containers are up and running
7. Application can be loaded on http://localhost:3000

## Design

![image](/img/CarCarDiagram.png)

## Service microservice

Utilizing both api and a polling service, this app renders the ability to view and set appointments as well as view and create technicians. The backend using Django framework and backend will use Javascript and React.

In the Appointments service, the backend is built with the Django framework using both a Django project (service_project) and app (service_rest). The app portion controls the ability to read, create, update, and delete functionality for the frontend. In this service you can create and view all appointments in the database. Additionally you can filter appointments using the customers (cx_name) vin to generate all appointment both past and present, this is done by using an api to pull all of the data attached to the Appointment model and rendering it in the frontend with React and bootsrap components. The React functions are fetching the data and the html is rendering it on the webpage. In the frontent you can see that the urls assigned to each service are attached using the APP file to route them from the urls file in the backend to thier appropriate javascript file in the frontend. All of this works just the same for the Technicians portion of this service.

Along with all of the api functionality, this service uses a poller to generate a very specific functionality. Being that the vin is a unique identifier and the application intends to treat cars purchased at the dealership(in the Inventory) differently than outside sales cars, we had to poll the vin in order to use this value object in a new way with maintaining the integrity of the data. The poller makes a copy of the inventory database for the vin and the frontend functions use that copy, using a function on the model encoder, to allow us to check if the vin is a car purchased at the dealership adding it to the inventory database, or if the appointment was created on the frontend and the car is an outside sale.

## Sales microservice

Using Django model, views and urls, the backend portion of the microservice allows a user
to create customers, sales records and sales people. This api pulls vehicle information like,
model, manufacturer, VIN, etc, from the Inventory API. There are four models in the Sales
microservice, AutomobileVO, PotentialCustomer, SalesPerson, and SalesRecord. A poller is used to
fetch data from the Inventory API and stores vehicle information in the AutomobileVO model.
The AutomobileVO is a value object. The customer sales person and records would be the entities.

The frontend portion of this service uses Javascript and React. The frontend fetches the data
from the database and loads it in the browser when requested and when new objects are being
created it takes that information and loads it into JSON format and stores it back in the
database.


#### Fully functional RESTful endpoints for the following entities at the following URLs:

The Sales API can be accessed at port 8090
The Inventory API can be accessed at port 8100

From Insomnia, you can access the sales endpoints at the following URLs.

## SALES

#### SALES:

| Action            | Method | URL                                   |
|-------------------|--------|---------------------------------------|
| List sales        | GET    | `http://localhost:8090/api/sales/`    |
| Create a sale     | POST   | `http://localhost:8090/api/sales/`    |
| List sale details | GET    | `http://localhost:8090/api/sales/:id/`|
| Delete a sale     | DELETE | `http://localhost:8090/api/sales/:id/`|

Creating a sales record requires the automobile VIN, price, sales_person id and customer id


            {
                "automobile": "1F0140FDS25235634",
                "price": 12345,
                "sales_person": 555,
                "customer": 5
            }


#### SALES PERSON:

| Action                   | Method | URL                                            |
|--------------------------|--------|------------------------------------------------|
| List sales person        | GET    | `http://localhost:8090/api/sales/employee`     |
| Create a sales person    | POST   | `http://localhost:8090/api/sales/employee`     |
| List sale person details | GET    | `http://localhost:8090/api/sales/employee/:id/`|
| Delete a sales person    | DELETE | `http://localhost:8090/api/sales/employee/:id/`|

Creating a sales person requires a name and id

            {
                "name": "DAVE",
                "id": 5
            }

#### CUSTOMER:

| Action                | Method | URL                                      |
|-----------------------|--------|------------------------------------------|
| List customers        | GET    | `http://localhost:8090/api/customer`     |
| Create a customer     | POST   | `http://localhost:8090/api/customer`     |
| List customer details | GET    | `http://localhost:8090/api/customer/:id/`|
| Delete a customer     | DELETE | `http://localhost:8090/api/customer/:id/`|

Creating a new customer requires a name, address, and phone_number

            {
                "name": "Carmen",
                "address": "777 seventh street",
                "phone_number": "777-777-7777",
            }

## SERVICES:

#### APPOINTMENTS:

| Action            | Method | URL                                          |
|-------------------|--------|----------------------------------------------|
| List Appointments | GET    | `http://localhost:8080/api/appointments/`    |
| Create an Appt    | POST   | `http://localhost:8080/api/appointments/`    |
| List appt details | GET    | `http://localhost:8080/api/appointments/:id/`|
| Delete an Appt    | DELETE | `http://localhost:8080/api/appointments/:id/`|

Creating an appointment record requires the automobile VIN, cx_name, date_time, service_type, and technician


            {
                "cx_name": "Josh",
                "date_time": "2027-10-27 14:30",
                "service_type": "Tire Rotation",
                "vin": "appointmentsBLUES",
                "technician": "Caroline"
            }



#### TECHNICIANS:

| Action            | Method | URL                                          |
|-------------------|--------|----------------------------------------------|
| List Technicians  | GET    | `http://localhost:8080/api/technicians/`     |
| Create an Tech    | POST   | `http://localhost:8080/api/technicians/`     |

Creating a technician record requires the name and a unique id of the employee

            {
                "tech_name": "Bill",
                "tech_id": 1
            }



## INVENTORY

#### MANUFACTURER:

| Action                    | Method | URL                                           |
|---------------------------|--------|-----------------------------------------------|
| List manufacturers        | GET    | `http://localhost:8100/api/manufacturers/`    |
| Create a manufacturer     | POST   | `http://localhost:8100/api/manufacturers/`    |
| List manufacturer details | GET    | `http://localhost:8100/api/manufacturers/:id/`|
| Edit a manufacturer       | PUT    | `http://localhost:8100/api/manufacturers/:id/`|
| Delete a manufacturer     | DELETE | `http://localhost:8100/api/manufacturers/:id/`|

Creating and updating a manufacturer requires only the manufacturer's name.

            {
                "name": "Chrysler"
            }

#### MODEL:

| Action             | Method | URL                                    |
|--------------------|--------|----------------------------------------|
| List models        | GET    | `http://localhost:8100/api/models/`    |
| Create a model     | POST   | `http://localhost:8100/api/models/`    |
| List model details | GET    | `http://localhost:8100/api/models/:id/`|
| Edit a model       | PUT    | `http://localhost:8100/api/models/:id/`|
| Delete a model     | DELETE | `http://localhost:8100/api/models/:id/`|

Creating and updating a vehicle model requires the model name, a URL of an image, and the id of the manufacturer.

            {
                "name": "Sebring",
                "picture_url": `https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg`,
                "manufacturer_id": 1
            }

#### AUTOMOBILE:

| Action                  | Method | URL                                         |
|-------------------------|--------|---------------------------------------------|
| List automobiles        | GET    | `http://localhost:8100/api/automobiles/`    |
| Create an automobile    | POST   | `http://localhost:8100/api/automobiles/`    |
| List automobile details | GET    | `http://localhost:8100/api/automobiles/:id/`|
| Edit an automobile      | PUT    | `http://localhost:8100/api/automobiles/:id/`|
| Delete an automobile    | DELETE

You can create an automobile with its color, year, VIN, and the id of the vehicle model.

            {
            "color": "red",
            "year": 2012,
            "vin": "1C3CC5FB2AN120174",
            "model_id": 1
            }
