from django.urls import path

from .views import (
    api_list_sales,
    api_sale_details,
    api_sales_person_list,
    api_employee_sales_list,
    api_customer_list,
    api_customer_details,
)

urlpatterns = [
    path("sales/", api_list_sales, name="api_list_sales"),
    path("sales/<int:pk>/", api_sale_details, name="api_sale_details"),
    path("sales/employee/", api_sales_person_list, name="api_sales_person_list"),
    path("sales/employee/<int:pk>/", api_employee_sales_list, name="employee_sales_list"),
    path("sales/customer/", api_customer_list, name="api_create_customer"),
    path("sales/customer/<int:pk>/", api_customer_details, name="api_customer_details"),
]
