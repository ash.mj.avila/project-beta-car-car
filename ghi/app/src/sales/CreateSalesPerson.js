import React, { useState } from 'react';

function CreateSalesPerson() {
    const [salesPersonName, setSalesPersonName] = useState('');
    const [id, setId] = useState('');

    const handleSalesPersonNameChange = (event) => {
        setSalesPersonName(event.target.value);
    }
    const handleIdChange = (event) => {
        setId(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name = salesPersonName;
        data.id = id;

        const salesPersonUrl = 'http://localhost:8090/api/sales/employee/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        }
        const response = await fetch(salesPersonUrl, fetchConfig);
        if (response.ok) {
            const newSalesPerson = await response.json();

            setSalesPersonName('');
            setId('');
        }
    }

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a New Sales Person</h1>
              <form onSubmit={handleSubmit} id="create-sales-person-form">
                <div className="form-floating mb-3">
                  <input onChange={handleSalesPersonNameChange} value={salesPersonName} placeholder="Sales person name" required type="text" name="sales_person_name" id="sales_person_name" className="form-control" />
                  <label forhtml="customer_name">Sales Person Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleIdChange} value={id} placeholder="Sales Person ID" type="text" name="id" id="id" className="form-control" />
                  <label forhtml="company_name">Sales Person ID</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
}

export default CreateSalesPerson
