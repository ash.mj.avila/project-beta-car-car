import React, { useEffect, useState } from "react";

function ManufacturerList() {

  const [manufacturers, setManufacturers] = useState([]);

  const fetchData = async () => {
    const url = `http://localhost:8100/api/manufacturers/`;
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }
  useEffect(() => {
    fetchData();
  }, [])

  return (
    <>
      <h2 className="gap-3 p-2 mt-3">Manufacturers</h2>
      <div className="gap-4 row pt- mb-3">
        {manufacturers.map(car => {
          return (
            <div key={car.id} value={car.id} className="shadow p-4 mt-4 card mb-3 shadow square border border-danger w-25">
              <img src='https://tinyurl.com/4wmcv4sb' className="card-img-top img-thumbnail img" />
              <div className="card-body">
                <h5 className="card-title">{car.name}</h5>
              </div>
            </div>
          );
        })}
      </div>
    </>
  )
}
export default ManufacturerList
